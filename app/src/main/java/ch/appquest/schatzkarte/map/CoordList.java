package ch.appquest.schatzkarte.map;


import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

import java.util.ArrayList;

import ch.appquest.schatzkarte.R;
import ch.appquest.schatzkarte.lib.SaveAndLoad;
import ch.appquest.schatzkarte.model.BackgroundMarker;

public class CoordList {
    private ArrayList<BackgroundMarker> points = new ArrayList<>();

    private static CoordList instance;

    private CoordList() {

    }

    public static CoordList getInstance() {
        if (instance == null) {
            instance = new CoordList();
        }
        return instance;
    }

    public Marker getMarker(int position, final MapView mapView) {
        final BackgroundMarker curr = points.get(position);

        Marker marker = new Marker(mapView);
        marker.setPosition(curr.getGeoPoint());
        marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        marker.setTitle(curr.getTitle());
        marker.setOnMarkerClickListener(new Marker.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(final Marker marker, final MapView mapView) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mapView.getContext());
                builder.setTitle(curr.getTitle())
                        .setMessage(curr.getDescription())
                        .setPositiveButton("Remove", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                remove(curr.getGeoPoint());
                                marker.remove(mapView);
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .setIcon(R.drawable.ic_location_on_blue_24dp)
                        .show();
                return false;
            }
        });
        marker.setOnMarkerDragListener(new Marker.OnMarkerDragListener() {
            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                GeoPoint position = marker.getPosition();
                curr.setGeoPoint(position);
                curr.setDescription("Latitude: " + Math.round(position.getLatitude() * 100000.0) / 1000000.0 +
                        "\nLongitude: " + Math.round(position.getLongitude() * 1000000.0) / 1000000.0);
                Toast.makeText(mapView.getContext(), "Updated marker position", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onMarkerDragStart(Marker marker) {

            }
        });
        marker.setIcon(mapView.getContext().getDrawable(R.drawable.ic_location_on_blue_24dp));
        marker.setDraggable(true);
        return marker;
    }

    public ArrayList<Marker> getMarkerList(final MapView mapView) {
        ArrayList<Marker> markers = new ArrayList<>();
        for (final BackgroundMarker curr : points) {
            Marker marker = new Marker(mapView);
            marker.setPosition(curr.getGeoPoint());
            marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
            marker.setTitle(curr.getTitle());
            marker.setSubDescription(curr.getDescription());
            marker.setOnMarkerClickListener(new Marker.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(final Marker marker, final MapView mapView) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mapView.getContext());
                    builder.setTitle(curr.getTitle())
                            .setMessage(curr.getDescription())
                            .setPositiveButton("Remove", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    remove(curr.getGeoPoint());
                                    marker.remove(mapView);
                                }
                            })
                            .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .setIcon(R.drawable.ic_location_on_blue_24dp)
                            .show();
                    return false;
                }
            });
            marker.setOnMarkerDragListener(new Marker.OnMarkerDragListener() {
                @Override
                public void onMarkerDrag(Marker marker) {

                }

                @Override
                public void onMarkerDragEnd(Marker marker) {
                    GeoPoint position = marker.getPosition();
                    curr.setGeoPoint(position);
                    curr.setDescription("Latitude: " + Math.round(position.getLatitude() * 100000.0) / 1000000.0 +
                            "\nLongitude: " + Math.round(position.getLongitude() * 1000000.0) / 1000000.0);
                    Toast.makeText(mapView.getContext(), "Updated marker position", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onMarkerDragStart(Marker marker) {

                }
            });
            marker.setIcon(mapView.getContext().getDrawable(R.drawable.ic_location_on_blue_24dp));
            markers.add(marker);
        }
        return markers;
    }

    public JSONArray exportMarkers() {
        JSONArray json = new JSONArray();
        for (BackgroundMarker curr : points) {
            JSONObject object = new JSONObject();
            try {
                object.put("lat", (long) (curr.getGeoPoint().getLatitude() * 1E6));
                object.put("lon", (long) (curr.getGeoPoint().getLongitude() * 1E6));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            json.put(object);
        }
        return json;
    }

    public void backup(Context context) {
        SaveAndLoad.saveData("data.tmp", exportMarkers().toString(), context);
    }

    public void restore(Context context) {
        String data = SaveAndLoad.loadData("data.tmp", context);

        JSONArray json;
        try {
            json = new JSONArray(data);
            for (int i = 0; i < json.length(); i++) {
                JSONObject curr = json.getJSONObject(i);
                GeoPoint position = new GeoPoint(curr.getDouble("lat") / 1E6, curr.getDouble("lon") / 1E6);
                add(position);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void add(GeoPoint position) {
        points.add(
                new BackgroundMarker(position,
                        "#" + (points.size() + 1),
                        "Latitude: " + Math.round(position.getLatitude() * 1000000.0) / 1000000.0 +
                                "\nLongitude: " + Math.round(position.getLongitude() * 1000000.0) / 1000000.0));
    }

    public void remove(GeoPoint geoPoint) {
        int i = 0;
        for (BackgroundMarker curr : points) {
            if (curr.getGeoPoint() == geoPoint) {
                points.remove(i);
                break;
            }
            i++;
        }
    }

    public int size() {
        return points.size();
    }
}
