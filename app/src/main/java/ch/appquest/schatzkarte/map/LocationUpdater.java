package ch.appquest.schatzkarte.map;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import java.util.Observable;

public class LocationUpdater extends Observable implements LocationListener {
    @Override
    public void onLocationChanged(Location location) {
        setChanged();
        notifyObservers(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
