package ch.appquest.schatzkarte.model;

import org.osmdroid.util.GeoPoint;

public class BackgroundMarker {
    private GeoPoint geoPoint;
    private String title;
    private String description;

    public BackgroundMarker(GeoPoint geoPoint, String title, String description) {
        this.geoPoint = geoPoint;
        this.title = title;
        this.description = description;
    }

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }

    public String getTitle() {
        return "Marker " + title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
