package ch.appquest.schatzkarte;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import ch.appquest.schatzkarte.map.CoordList;
import ch.appquest.schatzkarte.map.LocationUpdater;

public class MainActivity extends AppCompatActivity implements Observer {

    private static final long LOCATION_REFRESH_TIME = 0; //2 seconds
    private static final float LOCATION_REFRESH_DISTANCE = 0; //2 meters

    private MapView mapView;
    private IMapController mapController;
    private FloatingActionButton fab, fabInitalize;

    private LocationManager mLocationManager;
    private LocationUpdater locationUpdater;
    private MyLocationNewOverlay mLocationOverlay;
    private ItemizedOverlayWithFocus<OverlayItem> mOverlay;

    private GeoPoint curr_position = null;

    private CoordList coordList;

    private boolean AUTO_CENTER_TO_CURRENT_LOCATION = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        mapView = findViewById(R.id.map);
        fab = findViewById(R.id.fab);
        fabInitalize = findViewById(R.id.fabInitalize);

        setSupportActionBar(toolbar);

        //Init map
        mapView.setTileSource(TileSourceFactory.MAPNIK);

        mapView.setMaxZoomLevel(20);

        mapView.setMultiTouchControls(true);
        mapView.setBuiltInZoomControls(true);

        mapController = mapView.getController();
        mapController.setZoom(18);

        coordList = CoordList.getInstance();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (curr_position != null) {
                    coordList.add(curr_position);

                    Marker marker = coordList.getMarker(coordList.size() - 1, mapView);
                    mapView.getOverlays().add(marker);

                    Snackbar.make(view, "Marker added", Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(view, "No position found", Snackbar.LENGTH_LONG).show();
                }
            }
        });
        fabInitalize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                destroyListener();
                initLocationManager();
            }
        });

        getPermission(); //Get Location permission to show current location
    }

    private void getPermission() {
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                initLocationManager();
                try {
                    coordList.restore(MainActivity.this);
                    Toast.makeText(MainActivity.this, "Restored " + coordList.size() + " marker(s)", Toast.LENGTH_SHORT).show();
                } catch (Exception ex) {
                    Toast.makeText(MainActivity.this, "Restore failed or there was nothing to restore", Toast.LENGTH_SHORT).show();
                }
                initMarkers();
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(MainActivity.this, "Please grant access for location to use this app.", Toast.LENGTH_SHORT).show();
            }
        };

        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION)
                .check();
    }

    @SuppressLint("MissingPermission")
    private void initLocationManager() {
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        Location current;
        try {
            current = mLocationManager.getLastKnownLocation(mLocationManager.getBestProvider(new Criteria(), false));
        } catch (IllegalArgumentException ex) {
            current = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
        updateLocationOnMap(current);

        //Observer
        locationUpdater = new LocationUpdater();
        locationUpdater.addObserver(this);

        //Show my current Location
        mLocationOverlay = new MyLocationNewOverlay(new GpsMyLocationProvider(this), mapView);
        mLocationOverlay.enableMyLocation();
        mapView.getOverlays().add(this.mLocationOverlay);

        try {
            mLocationManager.requestLocationUpdates(mLocationManager.getBestProvider(new Criteria(), false), LOCATION_REFRESH_TIME,
                    LOCATION_REFRESH_DISTANCE, locationUpdater);
        } catch (IllegalArgumentException ex) {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_REFRESH_TIME,
                    LOCATION_REFRESH_DISTANCE, locationUpdater);
        }
    }

    private void destroyListener() {
        if (mLocationManager != null || locationUpdater != null) {
            mLocationManager.removeUpdates(locationUpdater);
            locationUpdater.deleteObserver(this);
        }
    }

    private int onPauseRuns = 0;

    @Override
    protected void onPause() {
        super.onPause();
        if (onPauseRuns != 0) {
            coordList.backup(this);
            destroyListener();
        }
        onPauseRuns++;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (TedPermission.isGranted(this, "ACCESS_FINE_LOCATION")) {
            initLocationManager();
        }
    }

    private void initMarkers() {
        mapView.getOverlays().clear();
        for (Marker curr : coordList.getMarkerList(mapView)) {
            //curr.setIcon(getDrawable(R.drawable.ic_location_on_blue_24dp));
            mapView.getOverlays().add(curr);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_disable_auto_center) {
            if (AUTO_CENTER_TO_CURRENT_LOCATION) {
                AUTO_CENTER_TO_CURRENT_LOCATION = false;
                Toast.makeText(this, "Disabled", Toast.LENGTH_SHORT).show();
            } else {
                AUTO_CENTER_TO_CURRENT_LOCATION = true;
                Toast.makeText(this, "Enabled", Toast.LENGTH_SHORT).show();
            }
            return true;
        } else if (id == R.id.action_log) {
            log();
        }

        return super.onOptionsItemSelected(item);
    }

    private void log() {
        Intent intent = new Intent("ch.appquest.intent.LOG");

        JSONArray data = CoordList.getInstance().exportMarkers();

        if (getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY).isEmpty()) {
            Toast.makeText(this, "Logbook App not Installed", Toast.LENGTH_LONG).show();
            return;
        }

        try {
            // Create JSON
            JSONObject jsonResult = new JSONObject();
            jsonResult.put("task", "Schatzkarte");
            jsonResult.put("points", data);
            intent.putExtra("ch.appquest.logmessage", jsonResult.toString());
        } catch (JSONException ex) {
            Toast.makeText(this, "Error while creating JSON", Toast.LENGTH_LONG).show();
            return;
        }
        startActivity(intent);
    }

    @Override
    public void update(Observable observable, Object o) {
        if (observable instanceof LocationListener) {
            if (o instanceof Location) {
                Location l = (Location) o;
                updateLocationOnMap(l);
            }
        }
    }

    private void updateLocationOnMap(Location l) {
        try {
            curr_position = new GeoPoint(l.getLatitude(), l.getLongitude());
            if (AUTO_CENTER_TO_CURRENT_LOCATION) {
                mapController.animateTo(new GeoPoint(l.getLatitude(), l.getLongitude(), l.getAltitude()));
            }
        } catch (NullPointerException ex) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }
}
